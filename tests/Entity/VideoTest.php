<?php

namespace App\Tests\Entity;

use App\Entity\Category;
use App\Entity\Style;
use App\Entity\Video;
use PHPUnit\Framework\TestCase;

class VideoTest extends TestCase
{
  public function testVideoIsCreated()
  {
    $video = new Video();
    $category = new Category();
    $style = new Style();
    $categoryName = $category->setCategoryName('webdocumentair');
    $styleName = $style->setStyleName('information');
    $video->setTitre('GeographicVR 360.mp4');
    $video->setCategory($categoryName);
    $video->setStyle($styleName);
    //On vérifie que $video est bien une instance de la classe Video
    $this->assertInstanceOf(Video::class, $video);
    //On vérifie que $CategoryName  est bien une instance de la classe Category
    $this->assertInstanceOf(Category::class, $categoryName);
    //On vérifie que  $StyleName est bien une instance de la classe Style
    $this->assertInstanceOf(Style::class, $styleName);
    //On vérifie que le Category soit correctement transformé en string
    $this->assertIsString($video->getCategory()->getCategoryName());
    //On vérifie que le Style soit correctement transformé en string
    $this->assertIsString($video->getStyle()->getStyleName());
    //On vérifie que $video a tous les attributs qui lui ont été définis
    $this->assertObjectHasAttribute('titre', $video);
    $this->assertObjectHasAttribute('price', $video);
    $this->assertObjectHasAttribute('category', $video);
    $this->assertObjectHasAttribute('style', $video);
    // On vérifie que les attributs sont ceux qui ont été définis
    $this->assertEquals($video->getTitre(), 'GeographicVR 360.mp4');
    $this->assertEquals($video->getCategory(), $categoryName);
    $this->assertEquals($video->getStyle(), $styleName);
  
  }
}

?>
