<?php

namespace App\Tests\Entity;

use App\Entity\User;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
  public function testUserIsCreated()
  {
    $user = new User();
    $user->setEmail('remy06@gregoire.fr');
    $user->setLastName('Guibert');
    $user->setFirstName('Élisabeth');
    $user->setProfileUser('Artisan');
    // On vérifie que $user est bien une instance de la classe User
    $this->assertInstanceOf(User::class, $user);
    // On vérie que $user a tous les attributs qui lui ont été définis
    $this->assertObjectHasAttribute('email', $user);
    $this->assertObjectHasAttribute('lastName', $user);
    $this->assertObjectHasAttribute('firstName', $user);
    $this->assertObjectHasAttribute('profileuser', $user);
    // On vérifie que les attributs sont ceux qui ont été définis
    $this->assertEquals($user->getEmail(), 'remy06@gregoire.fr');
    $this->assertEquals($user->getLastName(), 'Guibert');
    $this->assertEquals($user->getFirstName(), 'Élisabeth');
    $this->assertEquals($user->getProfileUser(), 'Artisan');
    
  }
}

?>
