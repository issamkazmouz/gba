//accordeon timeline
$(document).ready(function() {
  $('.choice-items > div').on('click', f_acc);
  dataChoice = '';
});

window.f_acc = function(){
  fillItems();

  $('.timeline-choice').css({paddingTop : "0"});

  var data = this.dataset.choice;
  if (dataChoice == '') {
      dataChoice = data;                
      $('.timeline-choice > div[data-choice="' + data + '"]').slideToggle({ duration: 500, ease: "easeInOutCubic" });
  } 
  if (dataChoice !== '' && dataChoice !== data) {
      dataChoice = data;
      var timelines = document.querySelectorAll('.timeline-choice > div');

      for (var i = 0; i < timelines.length; i++) {
          var thetimeline = timelines[i];
          $(thetimeline).slideToggle({ duration: 500, ease: "easeInOutCubic" });
      } 
  }
}

// scroll and fill the items
$(document).ready(function() {
    $(window).scroll( function() {
      fillItems();
    });
});

function fillItems() {
  // ajoute la classe "view" quand on scroll vers le bas
  $('div.timeline-item').each( function(i){
    // position de l'élément
    var bottom_of_element = $(this).offset().top;
    // position du scroll + la moitié de la hauteur de l'écran 
    var bottom_of_window = $(window).scrollTop() + $(window).height()/2;

    // console.log('');
    // console.log(bottom_of_element);
    // console.log(bottom_of_window);
    
    if( bottom_of_window > bottom_of_element ){
        $(this).addClass('view');
    }
  }); 
  // quand on scroll vers le haut, enlève la classe "view" 
  $('div.timeline-item.view').each( function(i){
    var bottom_of_element = $(this).offset().top;
    var bottom_of_window = $(window).scrollTop() + $(window).height()/2;
    
    if( bottom_of_window < bottom_of_element ){
        $(this).removeClass('view');
    }
  }); 
}
