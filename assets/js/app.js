/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you require will output into a single css file (app.css in this case)
require('../css/app.css');

// Need jQuery? Install it with "yarn add jquery", then uncomment to require it.
const $ = require('jquery');

global.$ = global.jQuery = $;

// Navigation mobile

var btnMobile = document.querySelector('#container-navigation-mobile .button-toggle');

var navMobile = document.querySelector('#container-navigation-mobile > #navigation-mobile');

if(btnMobile != null) {

btnMobile.addEventListener('click', function() {
    navMobile.classList.toggle('hidden');
})
}






var btn = $('#buttontop');
$(document).scroll(function (event) {
  posScroll = $(document).scrollTop();
  if (posScroll > 800) {
    btn.addClass('show')
  } else {
    btn.removeClass('show')
  }
});


btn.on('click', function(e) {
  e.preventDefault();
  $('html, body').animate({scrollTop:0}, '300');
});
