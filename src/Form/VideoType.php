<?php

namespace App\Form;

use App\Entity\Video;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Entity\User;
use App\Controller\UserController;
use App\Entity\Category;
use App\Entity\Style;
use App\Repository\CategoryRepository;
use App\Repository\StyleRepository;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Validator\Constraints\File;


class VideoType extends AbstractType
{
    public function getConfiguration($label, $placeholder, $isTrue)
    {
        return [
            'label' => $label,
            'required' => $isTrue,
            'attr' => [
                'placeholder' => $placeholder
            ]
        ];
    }
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
           
            //->add('titre', TextareaType::class, [
                
           ->add('titre', FileType::class, [
               'data_class' => null,
               'required'=>false,
               'mapped' => false,
              // 'data' => 'Hidden',
              // 'label' => 'titre ou name du vedio',
               'disabled' => 'false',
               'constraints' => [
                    new File([
                        'mimeTypes' => [
                            'video/mp4',
                            'video/mpeg' 
                        ],
                        'mimeTypesMessage' => 'Votre video doit être de type mp4',
                        'maxSize' => '40M',
                        'maxSizeMessage' => 'Votre video ne doit pas dépasser 40MB'
                    ])
                ]
           ])
           
            ->add('description', TextareaType::class, [
                'attr' => [
                    'rows' => '5',
                    'cols' => '8'
                ],

                'label' => 'Description',
            ])
            ->add('category', EntityType::class, [
                'class' => Category::class,
                'choice_label' => 'CategoryName',
                'choices' => $options['availableCategory']
            ])
            //->add('style')
            ->add('style', EntityType::class, [
                'class' => Style::class,
                'choice_label' => 'StyleName',
                'choices' => $options['availableStyle']
            ])
            //->add('style')
            
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => video::class,
            'availableCategory' => null,
            'availableStyle' => null,
           'video' => null
        ]);

    }


}
