<?php

namespace App\Form;

use App\Entity\Comment;
use App\Entity\User;
use App\Entity\Video;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use DateTime;
use Doctrine\DBAL\Types\DateType;


class CommentType extends AbstractType
{
    public function getConfiguration($label, $placeholder, $isTrue)
    {
        return [
            'label' => $label,
            'required' => $isTrue,
            'attr' => [
                'placeholder' => $placeholder
            ]
        ];
    }
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('rating', ChoiceType::class, [
            'choices' => [
                '1' => 1,
                '2' => 2,
                '3' => 3,
                '4' => 4,
                '5' => 5
            ],
            'label' => 'Note'
        ])
           // ->add('createdAt')
         ->add('comment', TextareaType::class, [
                'attr' => [
                    'rows' => '4',
                    'cols' => '5',
                ]
                ]); 
                $this->getConfiguration('Commentaire', 'Votre commentaire...', true)
            //->add('User')
            //->add('video')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Comment::class,
            'user' => null
        ]);
    }
}
