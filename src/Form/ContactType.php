<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Validator\Constraints\IsTrue;

class ContactType extends AbstractType
{
    public function getConfiguration($label, $placeholder, $isTrue)
    {
        return [
            'label' => $label,
            'required' => $isTrue,
            'attr' => [
                'placeholder' => $placeholder
            ]
        ];
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstname', TextType::class, $this->getConfiguration('Nom', 'Votre nom', true))
            ->add('lastname', TextType::class, $this->getConfiguration('Prénom', 'Votre prénom', true))
            ->add('phone', TextType::class, $this->getConfiguration('Téléphone', 'Votre numéro (facultatif)', false))
            ->add('email',  EmailType::class, $this->getConfiguration('Email', 'Votre adresse mail', true))
            ->add('message', TextareaType::class, $this->getConfiguration('Message', 'Saisissez votre message.', true));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
