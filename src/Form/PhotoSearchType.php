<?php

namespace App\Form;

use App\Entity\PhotoType;
use App\Entity\Photo;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\ResetType;

class PhotoSearchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->setMethod('GET')

            ->add('Category', HiddenType::class, [
                'label' => 'Category of the video',
                'disabled' => false,
                'required' => false,
                'attr' => [
                    'placeholder' => 'Category of the Video',                  
                ]
            ])
            ->add('Style', HiddenType::class, [
                'label' => 'Style of the video',
                'disabled' => false,
                'required' => false,
                'attr' => [
                    'placeholder' => 'Style  of the Video',                  
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([

            'csrf_protection' => false
        ]);
    }

}


