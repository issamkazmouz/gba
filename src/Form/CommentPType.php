<?php

namespace App\Form;

use App\Entity\CommentP;
use App\Entity\User;
use App\Entity\Photo;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use DateTime;
use Doctrine\DBAL\Types\DateType;


class CommentPType extends AbstractType
{
    public function getConfiguration($label, $placeholder, $isTrue)
    {
        return [
            'label' => $label,
            'required' => $isTrue,
            'attr' => [
                'placeholder' => $placeholder
            ]
        ];
    }
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('rating', ChoiceType::class, [
            'choices' => [
                '1' => 1,
                '2' => 2,
                '3' => 3,
                '4' => 4,
                '5' => 5
            ],
            'label' => 'Note'
        ])
           // ->add('createdAt')
         ->add('comment', TextareaType::class, [
                'attr' => [
                    'rows' => '4',
                    'cols' => '5',
                ]
                ]); 
                $this->getConfiguration('Commentaire', 'Votre commentaire...', true)
            //->add('User')
            
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CommentP::class,
            'user' => null
        ]);
    }
}
