<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Validator\Constraints\GreaterThan;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;

class UserEditType extends AbstractType
{

    public function getConfiguration(string $label, bool $required = false, bool $mapped = true, $constraints = null)
    {
        if ($constraints == null) {
            $constraints = new NotNull();
        }
        $placeholder = mb_strtolower($label);
        return [
            'label' => $label,
            'attr' => ['placeholder' => 'Votre '.$placeholder ],
            'required' => $required,
            'mapped' => $mapped,
            'empty_data' => '',
            'constraints' => [
                new NotBlank([
                    'message' => 'Vous devez renseigner votre '.$placeholder
                ]),
                new NotNull([
                    'message' => 'Vous devez renseigner votre '.$placeholder
                ]),
                $constraints
            ]
        ];
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // dd($options);
        $builder
            ->add('firstName', TextType::class, $this->getConfiguration('Prénom'))
            ->add('lastName', TextType::class, $this->getConfiguration('Nom'))
        ;

        $user = $options['user'];
        if ($user) {
            if(in_array('ROLE_ADMIN', $user->getRoles())) {
                $builder->add('isReport', ChoiceType::class, [
                    'label' => 'Signalé',
                    'required' => false,
                    'choices' => [
                        '' => '',
                        'Oui' => true,
                        'Non' => false
                    ]
                ]);
            }
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'cascade_validation' => true,
            'user' => false
        ]);
    }
}
