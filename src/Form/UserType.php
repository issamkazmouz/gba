<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;
use Symfony\Component\Validator\Constraints\NotCompromisedPassword;
use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\EqualTo;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class UserType extends AbstractType
{
    
    public function getConfiguration(string $label, bool $required = false, bool $mapped = true, $constraints = null)
    {
        if ($constraints == null) {
            $constraints = new NotNull();
        }
        $placeholder = mb_strtolower($label);
        return [
            'label' => $label,
            'attr' => ['placeholder' => 'Votre '.$placeholder ],
            'required' => $required,
            'mapped' => $mapped,
            'empty_data' => '',
            'constraints' => [
                new NotBlank([
                    'message' => 'Vous devez renseigner votre '.$placeholder
                ]),
                new NotNull([
                    'message' => 'Vous devez renseigner votre '.$placeholder
                ]),
                $constraints
            ]
        ];
    }
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('firstName', TextType::class, $this->getConfiguration('Prénom'))
        ->add('lastName', TextType::class, $this->getConfiguration('Nom'))
        ->add('email', TextType::class, $this->getConfiguration('E-mail', true, true, new Email()))
        //->add('profileUser', TextType::class, $this->getConfiguration('ProfileUser'))
        ->add('profileUser', ChoiceType::class,[
                   'choices' => ['Artisan'=>'Artisan','Visiteur'=>'Visiteur','Collectivite'=>'Collectivite','Autre'=>'Autre'],
               ])
        ->add('password', RepeatedType::class, [
            'type' => PasswordType::class,
            'mapped' => false,
            'invalid_message' => 'Les mots de passe ne correspondent pas',
            'options' => ['attr' => ['class' => 'password-field']],
            'required' => true,
            'first_options'  => [
                'label' => 'Mot de passe',
                'attr' => ['placeholder' => 'Votre mot de passe'],
                'constraints' => [
                    new NotBlank([
                        'message' => 'Vous devez renseigner votre ancien mot de passe'
                    ]),
                    new NotNull([
                        'message' => 'Vous devez renseigner votre ancien mot de passe'
                    ]),
                    new NotCompromisedPassword([
                        'message' => 'Votre mot de passe est trop faible'
                    ])
                ]
            ],
            'second_options' => [
                'label' => 'Confirmation du mot de passe',
                'attr' => ['placeholder' => 'Confirmez votre mot de passe']
            ]
        ])
            
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
