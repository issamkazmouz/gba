<?php

namespace App\DataFixtures;

use App\Entity\Comment;
use Faker\Factory;
use App\Entity\User;
use App\Entity\Category;
use App\Entity\Style;
use App\Repository\UserRepository;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Validator\Constraints\Length;

class AppFixtures extends Fixture
{
    private $encoder;
   // private $videoRepo;
    private $userRepo;
    public function __construct(UserPasswordEncoderInterface $encoder, UserRepository $userRepo)
    {
        $this->encoder = $encoder;
       // $this->videoRepo = $videoRepo;
        $this->userRepo = $userRepo;
    }

    public function load(ObjectManager $manager)
    {
        // Faker initialize
        $faker = Factory::create('fr_FR');

        // user ADMIN
        $userAdmin = new User();

        $userAdmin
            ->setRoles(['ROLE_USER', 'ROLE_ADMIN'])
            ->setLastName('KAZMOUZ')
            ->setfirstName('Issam')
            ->setEmail('issam.areha@gmail.com')
            ->setProfileUser('Admin')
            ->setPassword($this->encoder->encodePassword($userAdmin, 'passwordadmin'));


        $manager->persist($userAdmin);

        // users
        $users = [];
        $users[] = $userAdmin;

        for ($i = 0; $i < 52; $i++) {
            $user = new User();

            $profileUsers = array("Artisan", "Visiteur", "Collectivite", "Autre");
            $profileUser = $profileUsers[array_rand($profileUsers, 1)];
            $user
                ->setRoles(['ROLE_USER'])
                ->setLastName($faker->lastname)
                ->setfirstName($faker->firstname)
                ->setEmail($faker->email)
                ->setprofileUser($profileUser)
                ->setPassword($this->encoder->encodePassword($user, 'passworduser'));

            $manager->persist($user);
            $users[] = $user;
        }   

                 // Category
                 $categorys = [];
                 $categoryNames = [
                     'webdocumentaire',
                     'tutoriel',
                     'promotion',
                     'jeu',
                    ];
                 for ($i = 0; $i < count($categoryNames); $i++) {
                     $category = new Category();
         
                     $category->setCategoryName($categoryNames[$i]);
         
                     $manager->persist($category);
                     $categorys[] = $category;
                 } 


                                  // Style
                                  $styles = [];
                                  $styleNames = [
                                      'information',
                                      'formation',
                                      'patrimoine',
                                      'jeu',
                                      'tourisme',
                                      
                                  ];
                                  for ($i = 0; $i < count($styleNames); $i++) {
                                      $style = new Style();
                          
                                      $style->setStyleName($styleNames[$i]);
                          
                                      $manager->persist($style);
                                      $styles[] = $style;
                                  } 

                                            //Create user SUPER_ADMIN
    $superAdmin = new User();
    $superAdmin
    ->setRoles(['ROLE_SUPER_ADMIN', 'ROLE_ADMIN', 'ROLE_USER'])
    ->setLastName("Administrator")
    ->setfirstName("Admin")
    ->setEmail("gbavedio3d@gmail.com")
    ->setProfileUser('Admin')
    ->setPassword($this->encoder->encodePassword($superAdmin, 'passwordadmin'));
    

    $manager->persist($superAdmin);
    $manager->flush();
    }

 
}

    
