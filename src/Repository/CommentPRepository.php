<?php

namespace App\Repository;

use App\Entity\CommentP;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CommentP|null find($id, $lockMode = null, $lockVersion = null)
 * @method CommentP|null findOneBy(array $criteria, array $orderBy = null)
 * @method CommentP[]    findAll()
 * @method CommentP[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CommentPRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CommentP::class);
    }
    public function findByPhotoo( $photo = null)
    {
        $request = $this->createQueryBuilder('c');
 
        if($photo != null)
        {
           
            $request->andwhere('c.photo = :photo')
                    ->orderBy("c.createdAt", 'DESC')
                    ->setParameter('photo', $photo);
        }
       
       

        return $request->getQuery()->getResult();
    }

    // /**
    //  * @return CommentP[] Returns an array of CommentP objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CommentP
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
