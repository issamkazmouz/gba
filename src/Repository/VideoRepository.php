<?php

namespace App\Repository;

use App\Entity\Video;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Video|null find($id, $lockMode = null, $lockVersion = null)
 * @method Video|null findOneBy(array $criteria, array $orderBy = null)
 * @method Video[]    findAll()
 * @method Video[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VideoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Video::class);
    }


    public function findById($id): ?video
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findByCategoryAndStyle($category = null, $style = null)
    {
        $request = $this->createQueryBuilder('v');
      //  ->innerJoin('v.category', 'c')
       // ->innerJoin('v.style', 's');
        if($category != null)
        {
           $request->Where('v.category = :category')
               ->orderBy("v.createdAt", 'DESC')
               ->setParameter('category', $category);
        
        } 
        
        if($style != null)
        {
           
            $request->andwhere('v.style = :style')
                    ->orderBy("v.createdAt", 'DESC')
                    ->setParameter('style', $style);
        }
       
       

        return $request->getQuery()->getResult();
    }


    // /**
    //  * @return Video[] Returns an array of Video objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('v.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Video
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

 
}
