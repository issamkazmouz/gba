<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
//use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/** @UniqueEntity(fields={"titre"}, message="There is already an vedio with this titre") */
/**
 * @ORM\Entity(repositoryClass="App\Repository\VideoRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Video
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $titre;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="videos")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Category", inversedBy="videos")
     * @ORM\JoinColumn(nullable=false)
     */
    private $category;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Style", inversedBy="videos")
     * @ORM\JoinColumn(nullable=false)
     */
    private $style;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Comment", mappedBy="video" , cascade={"remove"})
     */
    private $comments;

    public function __construct()
    {
        $this->comments = new ArrayCollection();
    }
    /**
     * @ORM\PrePersist
     */
    public function prePersist()
    {
            $this->createdAt = new \DateTime();

    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getStyle(): ?Style
    {
        return $this->style;
    }

    public function setStyle(?Style $style): self
    {
        $this->style = $style;

        return $this;
    }
    public function __toString()
    {

        return $this->getTitre();
        
    }
    /**
     * Retourne la moyenne arrondies des notes des services effectués
     */
    /*
    public function getAverageRating()
    {
        $ratingSum = 0;
        $index = 0;
        $videooffs = $this->getVideo();
       
        foreach ($videooffs as $videooff) {
            if ( $videooffs->getComment() !== null) {
                $comment = $videooffs->getComment();

                $rating = $comment->getRating();
                $ratingSum = $ratingSum + $rating;
                $index = $index + 1;
            }
        }
        if ($index > 0) {
            $averageRating = $ratingSum/$index;
        } else {
            return false;
        }
        return round($averageRating);
    }
*/

        /**
     * S'il y a des commentaires
     * @return boolean
     */
    /*
    public function isComment()
    {
        $serviceOrders = $this->getVideo();
        $index = 0;

        foreach ($serviceOrders as $serviceOrder) {

            if ($serviceOrder->getComment()) {
                $index++;
            }
        }
     
        if ($index > 0) {
            return true; 
        } else {
            return false;
        }
    }
*/
    /**
     * @return Collection|Comment[]
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(Comment $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments[] = $comment;
            $comment->setVideo($this);
        }

        return $this;
    }

    public function removeComment(Comment $comment): self
    {
        if ($this->comments->contains($comment)) {
            $this->comments->removeElement($comment);
            // set the owning side to null (unless already changed)
            if ($comment->getVideo() === $this) {
                $comment->setVideo(null);
            }
        }

        return $this;
    }
}
