<?php

namespace App\Entity;

use App\Repository\PhotoRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PhotoRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class Photo
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $titre;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity=USER::class, inversedBy="photos")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity=Category::class, inversedBy="photos")
     */
    private $category;

    /**
     * @ORM\ManyToOne(targetEntity=Style::class, inversedBy="photos")
     */
    private $style;

    /**
     * @ORM\OneToMany(targetEntity=CommentP::class, mappedBy="photo")
     */
    private $commentPs;

    public function __construct()
    {
        $this->commentPs = new ArrayCollection();
    }
     /**
     * @ORM\PrePersist
     */
    public function prePersist()
    {
            $this->createdAt = new \DateTime();

    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUser(): ?USER
    {
        return $this->user;
    }

    public function setUser(?USER $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getStyle(): ?Style
    {
        return $this->style;
    }

    public function setStyle(?Style $style): self
    {
        $this->style = $style;

        return $this;
    }
    public function __toString()
    {

        return $this->getTitre();
        
    }

    /**
     * @return Collection|CommentP[]
     */
    public function getCommentPs(): Collection
    {
        return $this->commentPs;
    }

    public function addCommentP(CommentP $commentP): self
    {
        if (!$this->commentPs->contains($commentP)) {
            $this->commentPs[] = $commentP;
            $commentP->setPhoto($this);
        }

        return $this;
    }

    public function removeCommentP(CommentP $commentP): self
    {
        if ($this->commentPs->contains($commentP)) {
            $this->commentPs->removeElement($commentP);
            // set the owning side to null (unless already changed)
            if ($commentP->getPhoto() === $this) {
                $commentP->setPhoto(null);
            }
        }

        return $this;
    }
}
