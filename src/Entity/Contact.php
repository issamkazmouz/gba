<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;

class Contact {

    /**
     * @var string|null
     * @Assert\NotBlank()
     * @Assert\Length(min=2, max=100, minMessage="Le champ nom doit contenir 2 caractères au minimum.",
     * maxMessage="Le champ nom doit contenir 25 caractères au maximum.")
     */
    private $firstname;

    /**
     * @var string|null
     * @Assert\NotBlank()
     * @Assert\Length(min=2, max=100, minMessage="Le champ prénom doit contenir 2 caractères au minimum.",
     * maxMessage="Le champ nom doit contenir 25 caractères au maximum.")
     */
    private $lastname;

    /**
     * @var string|null
     * @Assert\Length(min=2, max=100, minMessage="Le champ téléphone doit contenir 2 caractères au minimum.",
     * maxMessage="Le champ nom doit contenir 25 caractères au maximum.")
     */
    private $phone;

    /**
     * @var string|null
     * @Assert\NotBlank()
     * @Assert\Length(min=2, max=100, minMessage="Le champ email doit contenir 2 caractères au minimum.",
     * maxMessage="Le champ nom doit contenir 50 caractères au maximum.")
     * @Assert\Email() 
     */
    private $email;
    
    /**
     * @var string|null
     * @Assert\NotBlank()
     * @Assert\Length(min=20, max=500, minMessage="Le champ message doit contenir 20 caractères au minimum.",
     * maxMessage="Le champ nom doit contenir 500 caractères au maximum.")
     * 
     */
    private $message;




    /**
     * Get maxMessage="Le champ nom doit contenir 25 caractères au maximum.")
     *
     * @return  string|null
     */ 
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set maxMessage="Le champ nom doit contenir 25 caractères au maximum.")
     *
     * @param  string|null  $firstname  maxMessage="Le champ nom doit contenir 25 caractères au maximum.")
     *
     * @return  self
     */ 
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get maxMessage="Le champ nom doit contenir 25 caractères au maximum.")
     *
     * @return  string|null
     */ 
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set maxMessage="Le champ nom doit contenir 25 caractères au maximum.")
     *
     * @param  string|null  $lastname  maxMessage="Le champ nom doit contenir 25 caractères au maximum.")
     *
     * @return  self
     */ 
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get maxMessage="Le champ nom doit contenir 25 caractères au maximum.")
     *
     * @return  string|null
     */ 
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set maxMessage="Le champ nom doit contenir 25 caractères au maximum.")
     *
     * @param  string|null  $phone  maxMessage="Le champ nom doit contenir 25 caractères au maximum.")
     *
     * @return  self
     */ 
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get maxMessage="Le champ nom doit contenir 25 caractères au maximum.")
     *
     * @return  string|null
     */ 
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set maxMessage="Le champ nom doit contenir 25 caractères au maximum.")
     *
     * @param  string|null  $email  maxMessage="Le champ nom doit contenir 25 caractères au maximum.")
     *
     * @return  self
     */ 
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get maxMessage="Le champ nom doit contenir 500 caractères au maximum.")
     *
     * @return  string|null
     */ 
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set maxMessage="Le champ nom doit contenir 500 caractères au maximum.")
     *
     * @param  string|null  $message  maxMessage="Le champ nom doit contenir 500 caractères au maximum.")
     *
     * @return  self
     */ 
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }
}

