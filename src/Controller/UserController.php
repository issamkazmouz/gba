<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use App\Security\UserAuthenticator;
use App\Form\UserPasswordType;
use App\Form\PasswordRecoverType;
use App\Form\PasswordEdit;
use App\Repository\TokenRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;
use App\Entity\Token;


/**
 * @Route("/user")
 */
class UserController extends AbstractController
{
    /**
     * @Route("/", name="user_index", methods={"GET"})
     */
    public function index(UserRepository $userRepository): Response
    {
        return $this->render('user/index.html.twig', [
            'users' => $userRepository->findAll(),
        ]);
    }

    /**
     * Permet à un nouvel utilisateur de s'inscrire
     * @Route("/new", name="user_new", methods={"GET","POST"})
     */
    public function new(Request $request, UserPasswordEncoderInterface $passwordEncoder, GuardAuthenticatorHandler $guardHandler, UserAuthenticator $authenticator): Response
    {
        $admin = $this->isGranted('ROLE_ADMIN');
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $user->setPassword(
                $passwordEncoder->encodePassword(
                    $user,
                    $form->get('password')->getData()
                ),
                $user->setRoles(["ROLE_USER"])
            );
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            $this->addFlash(
                'success',
                'Votre compte a été crée avec succès. Toute l\'équipe de AideTonVoisin vous souhaite la bienvenue.'
            );
            return $guardHandler->authenticateUserAndHandleSuccess(
                $user,
                $request,
                $authenticator,
                'main' // firewall name in security.yaml
            );
            if ($admin) {
                return $this->redirectToRoute('back_management_user');
            } else {
                return $this->redirectToRoute('home');
            }
            
        }

        return $this->render('user/new.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    private function authenticateUser(User $user)
    {
        $providerKey = 'secured_area'; // your firewall name
        $token = new UsernamePasswordToken($user, null, $providerKey, $user->getRoles());

        $this->container->get('security.context')->setToken($token);
    }

    /**
     * @Route("/{id}", name="user_show", methods={"GET"})
     */
    public function show(User $user): Response
    {
        return $this->render('user/show.html.twig', [
            'user' => $user,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="user_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, User $user): Response
    {
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('user_index');
        }

        return $this->render('user/edit.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="user_delete", methods={"DELETE"})
     */
    public function delete(Request $request, User $user): Response
    {
        $admin = $this->isGranted('ROLE_ADMIN');
        if (!$admin) {
            $this->addFlash(
                "danger",
                "Désolé, mais cette page n'éxiste pas."
            );
            return $this->redirectToRoute('home');
        }
        if ($this->isCsrfTokenValid('delete'.$user->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($user);
            $entityManager->flush();
        }

        return $this->redirectToRoute('user_index');
    }



        /**
     * Permet à un utilisateur de demander la création d'un nouveau mot de passe
     * @Route("/password/recovery/", name="password_recovery")
     */
    public function passwordRecovery(Request $request, \Swift_Mailer $mailer, UserRepository $repo, EntityManagerInterface $em)
    {
        $token = new Token();

        $form = $this->createForm(UserPasswordType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $repo->findOneBy(['email' => $form->get('email')->getData()]);
            $userId = $user->getId();
            $token->setNumber(bin2hex(random_bytes(60))); //On génère le token et on le transmet au setter.
            $token->setExpiresAt(new \DateTime('+5 minute'));
            $token->setUser($user);
            $em->persist($token); //On fais persister le token
            $em->flush(); //On l'enregistre en base de données
            $user = $form->get('email')->getData();
            $message = (new \Swift_Message('Récupèration de votre mot de passe'))
                ->setFrom('gbavedio3d@gmail.com')
                ->setTo($user)
                ->setBody(
                    $this->renderView(
                        'email/password.html.twig',
                        [
                            'email' => $user,
                            'token' => $token,
                            'id'    => $userId
                        ]
                    ),
                    'text/html'
                );

            $mailer->send($message);
            $this->addFlash(
                'success',
                'Votre mot de passe vient de vous être envoyé par email, pensez à vérifier vos spams.'

            );
            return $this->redirectToRoute('app_login');
        }

        return $this->render('user/password.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
