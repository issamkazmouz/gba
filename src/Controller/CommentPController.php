<?php

namespace App\Controller;

use App\Entity\CommentP;
use App\Entity\User;
use App\Entity\Photo;
use App\Form\CommentPType;
use App\Repository\CommentPRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/commentp")
 */
class CommentPController extends AbstractController
{
    /**
     * @Route("/", name="commentp_index", methods={"GET"})
     */
    public function index(CommentPRepository $commentPRepository): Response
    {
        $admin = $this->isGranted('ROLE_ADMIN');
        if (!$admin) {
            $this->addFlash(
                "danger",
                "Désolé, mais cette page n'éxiste pas."
            );
            return $this->redirectToRoute('home');
        }

        return $this->render('commentP/index.html.twig', [
            'comments' => $commentPRepository->findAll(),
        ]);
    }

    /**
     * Permet de poster un nouveau commentaire
     * @Route("/new", name="commentp_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $admin = $this->isGranted('ROLE_ADMIN');
        $user = $this->isGranted('ROLE_USER');
        if (!$admin && !$user) {
            $this->addFlash(
                "danger",
                "Désolé, mais cette page n'éxiste pas."
            );
            return $this->redirectToRoute('home');
        }
        $comment = new CommentP();
        $photo = new Photo();
        $user = new User();
        $form = $this->createForm(CommentPType::class, $comment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $comment->setPhotoo($photo);
            $comment->setUser($user);
            $comment->setCreatedAt(new \DateTime());

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($comment);
            $entityManager->flush();

            return $this->redirectToRoute('photo_show');
        }

        return $this->render('commentP/new.html.twig', [
            'comment' => $comment,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="commentp_show", methods={"GET"})
     */
    public function show(CommentP $comment ): Response
    {
        return $this->render('commentP/show.html.twig', [
            'comment' => $comment,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="commentp_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, CommentP $comment,$id): Response
    {
        $admin = $this->isGranted('ROLE_ADMIN');
        $user = $this->isGranted('ROLE_USER');
        if (!$admin && !$user) {
            $this->addFlash(
                "danger",
                "Désolé, mais cette page n'éxiste pas."
            );
            return $this->redirectToRoute('home');
        }
        $admin = $this->isGranted('ROLE_ADMIN');
        $form = $this->createForm(CommentPType::class, $comment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            if ($admin) {
                $this->addFlash(
                    "success",
                    "Le commentaire a correctement été édité."
                );
                return $this->redirectToRoute('back_management_commentp');
            } else {
                $this->addFlash(
                    "success",
                    "Votre commentaire a correctement été édité."
                );
            return $this->redirectToRoute('commentp_show',[
                'id' => $id,
            ]);
        }
    }
        return $this->render('commentP/edit.html.twig', [
            'comment' => $comment,
            'form' => $form->createView(),
            'user' => $this->getUser()->getRoles(),
        ]);
    }

    /**
     * @Route("/{id}", name="commentp_delete", methods={"DELETE"})
     */
    public function delete($id,Request $request, CommentP $comment,EntityManagerInterface $em, CommentPRepository $repo): Response
    {
        /*
        $admin = $this->isGranted('ROLE_ADMIN');
        if (!$admin) {
            $this->addFlash(
                "danger",
                "Désolé, mais cette page n'éxiste pas."
            );
            return $this->redirectToRoute('home');
        }
        */
        /*
        $deleteComment = $repo->find($id);
        $em->remove($deleteComment);
        //$em->persist();
        $em->flush();
        */
        
        if ($this->isCsrfTokenValid('delete'.$comment->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($comment);
            $entityManager->flush();
        }
        

        return $this->redirectToRoute('commentp_index');
    }
}
