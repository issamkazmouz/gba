<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Entity\Comment;
use App\Entity\CommentP;
use App\Entity\Video;
use App\Entity\Photo;
use App\Form\ChangeRoleType;
use App\Repository\UserRepository;
use App\Repository\CommentRepository;
use App\Repository\CommentPRepository;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\VideoRepository;
use App\Repository\PhotoRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


/**
 * Class BackManagementController
 * Contient toutes les fonctions du panneau d'administration
 * @package App\Controller
 */
class BackManagementController extends AbstractController
{
    /**
     * @param $request
     * @return int
     */
    public function page($request)
    {
        $page = $request->query->getInt('page');
        if ($page === 0) {
            $page = 1;
        }
        return $page;
    }

    
   /**
     * Fonction qui retourne les résultats de la base de données afin d'obtenir les statistiques de l'application depuis l'administration
     * @Route("/back", name="admin_index")
     */
    public function index(UserRepository $userRepo,VideoRepository $videoRepo,PhotoRepository $photoRepo,CommentRepository $commentRepo ,CommentPRepository $commentPRepo)
    {
        $admin = $this->isGranted('ROLE_ADMIN');
        if (!$admin) {
            $this->addFlash(
                "danger",
                "Désolé, mais cette page n'éxiste pas."
            );
            return $this->redirectToRoute('home');
        }
        
        $users = $userRepo->findAll();
        $video = $videoRepo->findAll();
        $photo = $photoRepo->findAll();
        $comment = $commentRepo->findAll();// on dois ajouter CommentRepository $commentRepo comme parameter
        $commentP = $commentPRepo->findAll();
        dump($comment);
        return $this->render('back_management/index.html.twig', [
            'users' => $users,
            'video' => $video,
            'comment' => $comment,
            'photo' => $photo,
            'commentp' => $commentP
        ]);
    }

    /**
     * Permet de gèrer les utilisateurs de l'application depuis l'administration
     * @Route("/back/management/user", name="back_management_user")
     */
    public function manageUser(Request $request, PaginatorInterface $paginator, Request $requestFormRoles)
    {
        $admin = $this->isGranted('ROLE_ADMIN');
        if (!$admin) {
            $this->addFlash(
                "danger",
                "Désolé, mais cette page n'éxiste pas."
            );
            return $this->redirectToRoute('home');
        }
        
        $user = new User();
       
        $page = $this->page($request);

        $userManagement = $this->getDoctrine()->getRepository(User::class)->findAll();

        $knp = $paginator->paginate(
            $userManagement,
            $page,
            50
        );
    
     
        return $this->render('back_management/userManagement.html.twig', [
            'management' => $knp,
            'knp' => $knp,
            'user' => $user,
            'leRole' => $user->getRoles()
        ]);
        
    }

    /**
     * Permet d'ajouter les droits administrateur à un utilisateur depuis l'administration
     * @Route("/back/management/editroleadmin/{id}", name="change_roleAdmin")
     */
    public function editRoleAdmin(User $user, EntityManagerInterface $em)
    {
        $admin = $this->isGranted('ROLE_ADMIN');
        if (!$admin) {
            $this->addFlash(
                "danger",
                "Désolé, mais cette page n'éxiste pas."
            );
            return $this->redirectToRoute('home');
        }
        $user->setRoles(array("ROLE_ADMIN", "ROLE_USER"));
        $em->persist($user);
        $em->flush();
        $lastName = $user->getLastName();
        $firstName = $user->getFirstName();
        $this->addFlash("danger",
        "Vous avez promu $lastName $firstName en tant qu'administrateur, il possède maintenant tous les droits.");
        return $this->redirectToRoute('back_management_user');
    }

    /**
     * Permet d'enlever les droits administrateur à un utilisateur depuis l'administration
     * @Route("/back/management/editroleuser/{id}", name="change_roleUser")
     */
    public function editRoleUser(User $user, EntityManagerInterface $em)
    {
        $admin = $this->isGranted('ROLE_ADMIN');
        if (!$admin) {
            $this->addFlash(
                "danger",
                "Désolé, mais cette page n'éxiste pas."
            );
            return $this->redirectToRoute('home');
        }
        $user->setRoles(array('ROLE_USER'));
        $em->persist($user);
        $em->flush();
        $lastName = $user->getLastName();
        $firstName = $user->getFirstName();
        $this->addFlash("success",
        "Vous avez promu $lastName $firstName en tant que simple utilisateur.");
        return $this->redirectToRoute('back_management_user');
    }

    /**
     * Permet de gérer les annonces depuis l'administration
     * @Route("/back/management/video", name="back_management_video")
     */
    public function manageVideoOffer(Request $request, PaginatorInterface $paginator)
    {
        $admin = $this->isGranted('ROLE_ADMIN');
        if (!$admin) {
            $this->addFlash(
                "danger",
                "Désolé, mais cette page n'éxiste pas."
            );
            return $this->redirectToRoute('home');
        }
        $page = $this->page($request);

        $videoManagement = $this->getDoctrine()->getRepository(Video::class)->findAll();
        
        $knp = $paginator->paginate(
            $videoManagement,
            $page,
            50
        );

        return $this->render('back_management/manageVideo.html.twig',[
            'management' => $knp,
            'knp' => $knp
        ]);
    }


    
    /**
     * Permet de gérer les annonces depuis l'administration
     * @Route("/back/management/photo", name="back_management_photo")
     */
    public function managePhotoOffer(Request $request, PaginatorInterface $paginator)
    {
        $admin = $this->isGranted('ROLE_ADMIN');
        if (!$admin) {
            $this->addFlash(
                "danger",
                "Désolé, mais cette page n'éxiste pas."
            );
            return $this->redirectToRoute('home');
        }
        $page = $this->page($request);

        $photoManagement = $this->getDoctrine()->getRepository(Photo::class)->findAll();
        
        $knp = $paginator->paginate(
            $photoManagement,
            $page,
            50
        );

        return $this->render('back_management/managePhoto.html.twig',[
            'management' => $knp,
            'knp' => $knp
        ]);
    }



    /**
    * * Permet de gérer les commentaires depuis l'administration
    * @Route("/back/management/commentp", name="back_management_commentp")
    */
    public function managerCommentp(Request $request, PaginatorInterface $paginator)
    {
        $admin = $this->isGranted('ROLE_ADMIN');
        if (!$admin) {
            $this->addFlash(
                "danger",
                "Désolé, mais cette page n'éxiste pas."
            );
            return $this->redirectToRoute('home');
        }

        $page = $this->page($request);

        $managementComment = $this->getDoctrine()->getRepository(CommentP::class)->findAll();    

        $knp = $paginator->paginate(
           $managementComment,
           $page,
           50 
        );

        return $this->render('back_management/managerCommentp.html.twig', [
            'management' => $knp,
            'knp' => $knp
        ]);

    }




    
    /**
    * * Permet de gérer les commentaires depuis l'administration
    * @Route("/back/management/comment", name="back_management_comment")
    */
    public function managerComment(Request $request, PaginatorInterface $paginator)
    {
        $admin = $this->isGranted('ROLE_ADMIN');
        if (!$admin) {
            $this->addFlash(
                "danger",
                "Désolé, mais cette page n'éxiste pas."
            );
            return $this->redirectToRoute('home');
        }

        $page = $this->page($request);

        $managementComment = $this->getDoctrine()->getRepository(Comment::class)->findAll();    

        $knp = $paginator->paginate(
           $managementComment,
           $page,
           50 
        );

        return $this->render('back_management/managerComment.html.twig', [
            'management' => $knp,
            'knp' => $knp
        ]);

    }
}


    /**
     * Permet de gérer les réservations depuis l'administration
     * @Route("/back/management/serviceorder", name="back_management_serviceOrder")
     */   /*
    public function managerServiceOrder(Request $request, ServiceOrderRepository $repo, PaginatorInterface $paginator)
   {
    $admin = $this->isGranted('ROLE_ADMIN');
        if (!$admin) {
            $this->addFlash(
                "danger",
                "Désolé, mais cette page n'éxiste pas."
            );
            return $this->redirectToRoute('home');
        }

        $page = $this->page($request);

        $serviceOrderManagement = $this->getDoctrine()->getRepository(ServiceOrder::class)->findAll();
        
        $knp = $paginator->paginate(
            $serviceOrderManagement,
            $page,
            50
        );


    return $this->render('back_management/manageServiceOrder.html.twig',[
        'management' => $knp,
        'knp' => $knp
    ]);
   }
   */


    


