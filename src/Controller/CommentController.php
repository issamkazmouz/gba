<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\User;
use App\Entity\Video;
use App\Form\CommentType;
use App\Repository\CommentRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/comment")
 */
class CommentController extends AbstractController
{
    /**
     * @Route("/", name="comment_index", methods={"GET"})
     */
    public function index(CommentRepository $commentRepository): Response
    {
        $admin = $this->isGranted('ROLE_ADMIN');
        if (!$admin) {
            $this->addFlash(
                "danger",
                "Désolé, mais cette page n'éxiste pas."
            );
            return $this->redirectToRoute('home');
        }

        return $this->render('comment/index.html.twig', [
            'comments' => $commentRepository->findAll(),
        ]);
    }

    /**
     * Permet de poster un nouveau commentaire
     * @Route("/new", name="comment_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $admin = $this->isGranted('ROLE_ADMIN');
        $user = $this->isGranted('ROLE_USER');
        if (!$admin && !$user) {
            $this->addFlash(
                "danger",
                "Désolé, mais cette page n'éxiste pas."
            );
            return $this->redirectToRoute('home');
        }
        $comment = new Comment();
        $video = new Video();
        $user = new User();
        $form = $this->createForm(CommentType::class, $comment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $comment->setVideo($video);
            $comment->setUser($user);
            $comment->setCreatedAt(new \DateTime());
            //$comment->setRating($form->get(rating)->getData());
            //$comment->setRating($form->get(comment)->getData());
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($comment);
            $entityManager->flush();

            return $this->redirectToRoute('video_show');
        }

        return $this->render('comment/new.html.twig', [
            'comment' => $comment,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="comment_show", methods={"GET"})
     */
    public function show(Comment $comment ): Response
    {
        return $this->render('comment/show.html.twig', [
            'comment' => $comment,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="comment_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Comment $comment,$id): Response
    {
        $admin = $this->isGranted('ROLE_ADMIN');
        $user = $this->isGranted('ROLE_USER');
        if (!$admin && !$user) {
            $this->addFlash(
                "danger",
                "Désolé, mais cette page n'éxiste pas."
            );
            return $this->redirectToRoute('home');
        }
        $admin = $this->isGranted('ROLE_ADMIN');
        $form = $this->createForm(CommentType::class, $comment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            if ($admin) {
                $this->addFlash(
                    "success",
                    "Le commentaire a correctement été édité."
                );
                return $this->redirectToRoute('back_management_comment');
            } else {
                $this->addFlash(
                    "success",
                    "Votre commentaire a correctement été édité."
                );
            return $this->redirectToRoute('comment_show',[
                'id' => $id,
            ]);
        }
    }
        return $this->render('comment/edit.html.twig', [
            'comment' => $comment,
            'form' => $form->createView(),
            'user' => $this->getUser()->getRoles(),
        ]);
    }

    /**
     * @Route("/{id}", name="comment_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Comment $comment,EntityManagerInterface $em, CommentRepository $repo): Response
    {
        $admin = $this->isGranted('ROLE_ADMIN');
        if (!$admin) {
            $this->addFlash(
                "danger",
                "Désolé, mais cette page n'éxiste pas."
            );
            return $this->redirectToRoute('home');
        }
        /*
        $deleteComment = $repo->find($id);
        $em->remove($deleteComment);
        $em->flush();
        */
    
        if ($this->isCsrfTokenValid('delete'.$comment->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($comment);
            $entityManager->flush();
        }
        

        return $this->redirectToRoute('comment_index');
    }
}
