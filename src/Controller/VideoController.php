<?php

namespace App\Controller;

use App\Entity\Video;
use App\Form\VideoType;
use App\Repository\VideoRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use DateTime;
use App\Entity\User;
use App\Entity\Comment;
use App\Form\CommentType;
use App\Form\UserType;
use App\Entity\Category;
use Doctrine\ORM\EntityManager;
use App\Repository\UserRepository;
use App\Repository\CommentRepository;
use App\Form\VideoSearchType;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\CategoryRepository;
use App\Repository\StyleRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\Form\FormTypeInterface;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;
use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination;



/**
 * @Route("/video")
 */
class VideoController extends AbstractController
{

    
    /**
     * Permet d'obtenir les résultats des annonces via le moteur de recherche
     * @Route("/", name="video_index", methods={"GET"})
     */
    public function index(Request $request, VideoRepository $videoRepo, CategoryRepository $category,StyleRepository $style, PaginatorInterface $paginator): Response
    {
        $admin = $this->isGranted('ROLE_ADMIN');
        $us = $this->isGranted('ROLE_USER');
        if (!$admin && !$us) {
            $this->addFlash(
                "danger",
                "Désolé, mais cette page n'éxiste pas."
            );
            return $this->redirectToRoute('home');
        }
        $page = $request->query->getInt('page');
        if ($page === 0) {
            $page = 1;
        }

        $form = $this->createForm(VideoSearchType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //On récupère la saisie faite dans le champ serviceType du formulaire de recherche

            $categoryType = $form->get('Category')->getData();
            //dump($categoryType);
            $categoryName = $category->findByCategoryName($categoryType);
            //dump($categoryName);
            $styleType = $form->get('Style')->getData();
           // dump($styleType);
            $styleName = $style->findByStyleName($styleType);
           // dump($styleName);
            // On met les valeurs des champs en paramètre de la fonction findByCityAndServiceType qui se trouve dans le RepositoryServiceOffer
            $videoOffer = $paginator->paginate(
                $videoRepo->findByCategoryAndStyle( $categoryName,$styleName),
                $page,
                15
            );
        } else {
            $videoOffer = $paginator->paginate(
                $videoRepo->findAll('DESC'),
                $page,
                15
            );
        }

        return $this->render('video/index.html.twig', [
            'video' => $videoOffer,
            'search' => $form->createView()
        ]);
    }



    /**
     * @Route("/new", name="video_new", methods={"POST","GET"})
     */
    public function new(Request $request,GuardAuthenticatorHandler $guardHandler): Response
    {
        $admin = $this->isGranted('ROLE_ADMIN');
        if (!$admin) {
            $this->addFlash(
                "danger",
                "Désolé, mais cette page n'éxiste pas."
            );
            return $this->redirectToRoute('home');
        }
        $video = new Video();
        $form = $this->createForm(VideoType::class, $video);
        $form->handleRequest($request);
  
        if ($form->isSubmitted() && $form->isValid()) {
        if(isset($_POST['upload']) && !empty($_POST['upload']) )
        {
            // $name = $form->get('titre')->getData();
            $name = $_FILES['file']['name'];
            $tmp = $_FILES['file']['tmp_name'];
            
            
     $name1= substr($name, -3);
            if($name1 !="mp4"){
                $this->addFlash(
                    "danger",
                    "Désolé, mais The Extention of the vedio must be mp4"
                );
                return $this->redirectToRoute('home');
            }
            
       

            move_uploaded_file($tmp,"./vedios/" . $name);
            // $tmp=$name;
            $file = $name; 
        } 
        //
        $video1 = $this->getDoctrine()->getRepository(Video::class)->findAll();
      for($i=0 ; $i<count($video1);$i++) {
          if($video1[$i]->getTitre()==$name){
            $this->addFlash(
                "danger",
                "Désolé, mais The Video " . $name . "was fond before"
                        );
            return $this->redirectToRoute('home');
        }
          }
      

        //
         $video->setTitre($name);
         
         $video->setUser($this->getUser());
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($video);
            $entityManager->flush();
            return $this->redirectToRoute('video_index');
        } 
        return $this->render('video/new.html.twig', [
            'video' => $video,
            'form' => $form->createView(),
        ]);
        
}


    /**
     * Permet d'afficher un video grâce à son ID
     * @Route("/{id}", name="video_show", methods={"GET","POST"})
     */
    public function show($id,Video $video , Request $request, EntityManagerInterface $entityManager,VideoRepository $repo,CommentRepository $repoComment,PaginatorInterface $paginator): Response
    {

        $admin = $this->isGranted('ROLE_ADMIN');
        $us = $this->isGranted('ROLE_USER');
        if (!$admin && !$us) {
            $this->addFlash(
                "danger",
                "Désolé, mais cette page n'éxiste pas."
            );
            return $this->redirectToRoute('home');
        }
   
        $comment = new Comment();
        $form = $this->createForm(CommentType::class, $comment);
        $form->handleRequest($request);
            

        if ($form->isSubmitted() && $form->isValid()) {
      
            $comment->setVideo($video);
            $comment->setUser($this->getUser());
            $comment->setCreatedAt(new \DateTime());
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($comment);
            $entityManager->flush();
     
         
            

            return $this->redirectToRoute('video_show',[
                'id'=>$id,
             
            ]);
        }
         
      
        return $this->render('video/show.html.twig', [
            'titre' => $video->getTitre(),
            'video' => $video,
            'comments' => $repoComment->findByVideo($video),
            'comment1' => $comment,
            'form' => $form->createView(),

        ]);
    }

    /**
     * @Route("/{id}/edit", name="video_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Video $video): Response
    {
        $admin = $this->isGranted('ROLE_ADMIN');
        if (!$admin) {
            $this->addFlash(
                "danger",
                "Désolé, mais cette page n'éxiste pas."
            );
            return $this->redirectToRoute('home');
        }
        $form = $this->createForm(VideoType::class, $video);
        $form->handleRequest($request);
       // $video = $repo->find($id);
        $file1 = $video->getTitre();
        //dump($file);


        //$_FILES['file']['value'] =;
        if ($form->isSubmitted() && $form->isValid()) {
         
            if(isset($_POST['upload']) && !empty($_POST['upload']) )
            {
                // $name = $form->get('titre')->getData();
                $name = $_FILES['file']['name'];
                $tmp = $_FILES['file']['tmp_name'];

                // the extention of the video must be mp4
                $name1= substr($name, -3);
                if($name1 !="mp4"){
                    $this->addFlash(
                        "danger",
                        "Désolé, mais The Extention of the vedio must be mp4"
                    );
                    return $this->redirectToRoute('home');
                }

            
                move_uploaded_file($tmp,"./vedios/" . $name);
                // $tmp=$name;
                $file = $name; 
            }
       if ($file !=""){
            $video->setTitre($file);
       }else{
        $video->setTitre($file1);
       }
         
            $video->setUser($this->getUser());
               $entityManager = $this->getDoctrine()->getManager();
               $entityManager->persist($video);
               $entityManager->flush();
               
            return $this->redirectToRoute('video_index');
        }

        return $this->render('video/edit.html.twig', [
            'video' => $video,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="video_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Video $video): Response
    {
        $admin = $this->isGranted('ROLE_ADMIN');
        if (!$admin) {
            $this->addFlash(
                "danger",
                "Désolé, mais cette page n'éxiste pas."
            );
            return $this->redirectToRoute('home');
        }

        if ($this->isCsrfTokenValid('delete'.$video->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($video);
            $entityManager->flush();
        }
        

        return $this->redirectToRoute('video_index');
    }
}
