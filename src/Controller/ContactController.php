<?php

namespace App\Controller;

use App\Entity\Contact;
use App\Form\ContactType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ContactController extends AbstractController
{
    /**
     * Permet d'envoyer un mail à l'administrateur depuis la formulaire de contact
     * @Route("/contact", name="contact")
     */
    public function index(Request $request, \Swift_Mailer $mailer)
    {
        $contact = new Contact();
        $form = $this->createForm(ContactType::class, $contact);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $contactFormData = $form->getData();
            //On construit le mail
            $message = (new \Swift_Message('Message à l\'administration de GBA'))
                ->setFrom($contact->getEmail()) //On récupère l'adresse mail de l'utilisateur
                ->setTo('gbavedio3d@gmail.com') //On renseigne l'adresse mail de l'administrateur
                ->setBody(
                    $this->renderView(
                        //On récupère et on insère les données du formulaire dans le mail
                        'email/index.html.twig',
                        [
                            'firstname' => $contact->getFirstname(),
                            'lastname' => $contact->getLastname(),
                            'phone' => $contact->getPhone(),
                            'email' => $contact->getEmail(),
                            'message' => $contact->getMessage()
                        ]
                    ),
                    'text/html'
                );

            $mailer->send($message);
            $this->addFlash(
                'success',
                'Votre message a bien été envoyé à l\'administrateur'
            );
            return $this->redirectToRoute('contact');
        }

        return $this->render('contact/index.html.twig', [
            'form' => $form->createView()
        ]);
    }

        /**
     * Permet d'envoyer un mail à l'administrateur depuis la formulaire de contact
     * @Route("contact/map", name="map")
     */
    public function map(Request $request)
    {
        return $this->render('contact/map.html.twig');
    }
}
