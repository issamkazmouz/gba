<?php

namespace App\Controller;

use App\Entity\Photo;
use App\Form\PhotoType;
use App\Repository\PhotoRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use DateTime;
use App\Entity\User;
use App\Entity\CommentP;
use App\Form\CommentPType;
use App\Form\UserType;
use App\Entity\Category;
use Doctrine\ORM\EntityManager;
use App\Repository\UserRepository;
use App\Repository\CommentPRepository;
use App\Form\PhotoSearchType;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\CategoryRepository;
use App\Repository\StyleRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\Form\FormTypeInterface;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;
use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination;



/**
 * @Route("/photo")
 */
class PhotoController extends AbstractController
{

    
    /**
     * Permet d'obtenir les résultats des annonces via le moteur de recherche
     * @Route("/", name="photo_index", methods={"GET"})
     */
    public function index(Request $request, PhotoRepository $photoRepo, CategoryRepository $category,StyleRepository $style, PaginatorInterface $paginator): Response
    {
        $admin = $this->isGranted('ROLE_ADMIN');
        $us = $this->isGranted('ROLE_USER');
        if (!$admin && !$us) {
            $this->addFlash(
                "danger",
                "Désolé, mais cette page n'éxiste pas."
            );
            return $this->redirectToRoute('home');
        }
        $page = $request->query->getInt('page');
        if ($page === 0) {
            $page = 1;
        }

        $form = $this->createForm(PhotoSearchType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //On récupère la saisie faite dans le champ serviceType du formulaire de recherche

            $categoryType = $form->get('Category')->getData();
            //dump($categoryType);
            $categoryName = $category->findByCategoryName($categoryType);
            //dump($categoryName);
            $styleType = $form->get('Style')->getData();
           // dump($styleType);
            $styleName = $style->findByStyleName($styleType);
           // dump($styleName);
            // On met les valeurs des champs en paramètre de la fonction findByCityAndServiceType qui se trouve dans le RepositoryServiceOffer
            $photoOffer = $paginator->paginate(
                $photoRepo->findByCategoryAndStyle( $categoryName,$styleName),
                $page,
                15
            );
        } else {
            $photoOffer = $paginator->paginate(
                $photoRepo->findAll('DESC'),
                $page,
                15
            );
        }

        return $this->render('photo/index.html.twig', [
            'photo' => $photoOffer,
            'search' => $form->createView()
        ]);
    }



    /**
     * @Route("/new", name="photo_new", methods={"POST","GET"})
     */
    public function new(Request $request,GuardAuthenticatorHandler $guardHandler): Response
    {
        $admin = $this->isGranted('ROLE_ADMIN');
        if (!$admin) {
            $this->addFlash(
                "danger",
                "Désolé, mais cette page n'éxiste pas."
            );
            return $this->redirectToRoute('home');
        }
        $photo = new Photo();
        $form = $this->createForm(PhotoType::class, $photo);
        $form->handleRequest($request);
  
        if ($form->isSubmitted() && $form->isValid()) {
        if(isset($_POST['upload']) && !empty($_POST['upload']) )
        {
            // $name = $form->get('titre')->getData();
            $name = $_FILES['file']['name'];
            $tmp = $_FILES['file']['tmp_name'];

            //the extention of the image must be jpg or jpeg 
            $name1= substr($name, -3);
            $name2= substr($name, -4);
            if($name1 !="jpg" && $name2 !="jpeg"){
                $this->addFlash(
                    "danger",
                    "Désolé, mais The Extention of the vedio must be jpg or jpeg"
                );
                return $this->redirectToRoute('home');
            }

       
        
            move_uploaded_file($tmp,"./photos/" . $name);
            // $tmp=$name;
            $file = $name; 
        } 
       //
       $photo1 = $this->getDoctrine()->getRepository(Photo::class)->findAll();
       for($i=0 ; $i<count($photo1);$i++) {
           if($photo1[$i]->getTitre()==$name){
             $this->addFlash(
                 "danger",
                 "Désolé, mais The Image " . $name . "was fond before"
                         );
             return $this->redirectToRoute('home');
         }
           }
       
 
         //
         $photo->setTitre($name);
         
         $photo->setUser($this->getUser());
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($photo);
            $entityManager->flush();
            return $this->redirectToRoute('photo_index');
        } 
        return $this->render('photo/new.html.twig', [
            'photo' => $photo,
            'form' => $form->createView(),
        ]);
        
}


    /**
     * Permet d'afficher une photo grâce à son ID
     * @Route("/{id}", name="photo_show", methods={"GET","POST"})
     */
    public function show($id,Photo $photo , Request $request, EntityManagerInterface $entityManager,PhotoRepository $repo,CommentPRepository $repoComment,PaginatorInterface $paginator): Response
    {

        $admin = $this->isGranted('ROLE_ADMIN');
        $us = $this->isGranted('ROLE_USER');
        if (!$admin && !$us) {
            $this->addFlash(
                "danger",
                "Désolé, mais cette page n'éxiste pas."
            );
            return $this->redirectToRoute('home');
        }
        $comment = new CommentP();
        $form = $this->createForm(CommentPType::class, $comment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $comment->setPhoto($photo);
           $comment->setUser($this->getUser());
            $comment->setCreatedAt(new \DateTime());
           $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($comment);
            $entityManager->flush();

            return $this->redirectToRoute('photo_show',[
                'id'=>$id,
             
            ]);
        }

        return $this->render('photo/show.html.twig', [
            'titre' => $photo->getTitre(),
            'photo' => $photo,
            'comments' => $repoComment->findByPhotoo($photo),
            'comment1' => $comment,
            'form' => $form->createView(),

        ]);
    }

    /**
     * @Route("/{id}/edit", name="photo_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Photo $photo): Response
    {
        $admin = $this->isGranted('ROLE_ADMIN');
        if (!$admin) {
            $this->addFlash(
                "danger",
                "Désolé, mais cette page n'éxiste pas."
            );
            return $this->redirectToRoute('home');
        }
        $form = $this->createForm(PhotoType::class, $photo);
        $form->handleRequest($request);
       
        $file1 = $photo->getTitre();
        //dump($file);


        //$_FILES['file']['value'] =;
        if ($form->isSubmitted() && $form->isValid()) {
         
            if(isset($_POST['upload']) && !empty($_POST['upload']) )
            {
                // $name = $form->get('titre')->getData();
                $name = $_FILES['file']['name'];
                $tmp = $_FILES['file']['tmp_name'];


                     //the extention of the image must be jpg or jpeg 
                     if($name !=""){
                        $name1= substr($name, -3);
                        $name2= substr($name, -4);
                     }else{
                        $name1= substr($file1, -3);
                        $name2= substr($file1, -4);          
                     }
          
            if($name1 !="jpg" && $name2 !="jpeg"){
                $this->addFlash(
                    "danger",
                    "Désolé, mais The Extention of the vedio must be jpg or jpeg"
                );
                return $this->redirectToRoute('home');
            }


            
                move_uploaded_file($tmp,"./photos/" . $name);
                // $tmp=$name;
                $file = $name; 
            }
       if ($file !=""){
            $photo->setTitre($file);
       }else{
        $photo->setTitre($file1);
       }
         
            $photo->setUser($this->getUser());
               $entityManager = $this->getDoctrine()->getManager();
               $entityManager->persist($photo);
               $entityManager->flush();
               
            return $this->redirectToRoute('photo_index');
        }

        return $this->render('photo/edit.html.twig', [
            'photo' => $photo,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="photo_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Photo $photo): Response
    {
        $admin = $this->isGranted('ROLE_ADMIN');
        if (!$admin) {
            $this->addFlash(
                "danger",
                "Désolé, mais cette page n'éxiste pas."
            );
            return $this->redirectToRoute('home');
        }

        if ($this->isCsrfTokenValid('delete'.$photo->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($photo);
            $entityManager->flush();
        }
        

        return $this->redirectToRoute('photo_index');
    }
}
