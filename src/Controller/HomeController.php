<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\UserRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\video;
use App\Form\VideoSearchType;
use App\Repository\CategoryRepository;
use App\Repository\VideoRepository;
use App\Repository\StyleRepository;

class HomeController extends AbstractController
{
    /**
     * @Route("/h", name="home1", methods={"GET","POST"})
     */
    public function index2()
    {
        return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController',
        ]);
    }

        /**
     * @Route("/player", name="player", methods={"GET","POST"})
     */
    public function index1()
    {
        return $this->render('home/jwplayer.html.twig', [
            'controller_name' => 'HomeController',
        ]);
    }
            /**
     * @Route("/photo1", name="photo3d", methods={"GET","POST"})
     */
    public function index3()
    {
        return $this->render('home/photo3d.html.twig', [
            'controller_name' => 'HomeController',
        ]);
    }
    /**
     * Permet d'obtenir les 6 meilleurs video et d'intégrer le formulaire de recherche sur la page d'accueil
     * @Route("/", name="home",methods={"GET","POST"})
     */
    public function index(Request $request, VideoRepository $videoRepo, CategoryRepository $category,StyleRepository $style, PaginatorInterface $paginator, UserRepository $userRepository)
    {
        $videoSearch = new video();
        $form = $this->createForm(VideoSearchType::class);
        $form->handleRequest($request);
        
        $page = $request->query->getInt('page');
        if ($page === 0) {
            $page = 1;
        }

        if ($form->isSubmitted() && $form->isValid()) {

            $category = $form->get('Category')->getData();
            $style = $form->get('Style')->getData();
            $categoryName = $category->findByCategoryName($category);
            $styleName = $style->findByStyleName($style);
            $videooffer = $paginator->paginate(
                $videoRepo->findByCategoryAndStyle($categoryName, $styleName),
                $page,
                15
                );     
            return $this->render('video/index.html.twig', [
                'search' => $form->createView(),
                'video' => $videooffer
            ]);    
        } 
  
        return $this->render('home/index.html.twig', [
            'search' => $form->createView(),
           

        ]);
    }

}
